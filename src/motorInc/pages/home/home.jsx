import React from "react";
//import { withTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import HomeWrapper from "./home.style";


const Home = (props) => {
 
  return (
    <HomeWrapper>
      <div className="vehicle-wrapper">
        MotorInc Home
      </div>
    </HomeWrapper>
  );
};

export default Home;