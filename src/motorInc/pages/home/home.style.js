import styled from 'styled-components';


const HomeWrapper = styled.div`

.vehical-section {
    margin-top: 20px;
    margin-bottom: 10px;
}
.policy-section {
    margin-top: 20px;
    margin-bottom: 20px;
}
.fit{
    // margin-top: 30vh;
    bottom: 55px;
    position: fixed;
    width: 100%;
}
@media only screen and (max-width: 576px) {
    .fit{
        margin-top: 4rem;
        position:relative;
        bottom:0;

    }

  }
`;


export default HomeWrapper;