
import { MotorIncHome  } from './motorInc';
const routes = [
  {
    name: "motorinc",
    key: "motorinc",
    route: "/motor-insurence",
    component: <MotorIncHome />,
  }
];

export default routes;
