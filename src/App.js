import React from 'react';
//import i18n from './translations';
//import { I18nextProvider } from 'react-i18next';
import { Routes, Route } from "react-router-dom";
//import { Provider } from 'react-redux';
import routes from "./routes";
import './assets/css/App.css';
import PageNotFound from './_shared/pages/PageNotFound';


function App() {
    const getRoutes = (allRoutes) =>
        allRoutes.map((route) => {
        if (route.route) {
            return <Route exact path={route.route} element={route.component} key={route.key} />;
        }
        return null;
    });

    return (
        // <Provider store={store}>
        //     <I18nextProvider i18n={i18n}>
                <div className="App">
                
                <Routes>
                    {getRoutes(routes)}
                    <Route path="*" component={PageNotFound} />
                </Routes>
             
                </div>
        //     </I18nextProvider>
        // </Provider>
    );
}

export default App;
