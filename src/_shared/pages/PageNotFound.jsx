import React from 'react';
import CommonPagesWrapper from './style';

const PageNotFound = (props) =>{
    return (
        <CommonPagesWrapper>
            404 Not Found
        </CommonPagesWrapper>
    );
}

export default PageNotFound;